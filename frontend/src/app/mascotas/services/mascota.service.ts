import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Mascota} from "../interfaces/mascota.interface";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MascotaService {

  private baseUrl: string = environment.baseUrl;

  constructor(private httpClient: HttpClient) {
  }

  guardarMascota(mascota:Mascota):Observable<number>{
    return this.httpClient.post<number>(this.baseUrl+'/mascotas',mascota);
    // TODO - API POST
  }

  getMascotas():Observable<Mascota[]>{
    return this.httpClient.get<Mascota[]>(this.baseUrl + '/mascotas');
    // TODO - API GET
  }
}
