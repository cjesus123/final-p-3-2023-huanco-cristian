import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Mascota} from "../../interfaces/mascota.interface";
import {MascotaService} from "../../services/mascota.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-mascota',
  templateUrl: './mascota.component.html',
  styleUrls: ['./mascota.component.css']
})
export class MascotaComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
    nombre: [undefined, [Validators.required, Validators.minLength(3)]],
    fecha_nacimiento: [undefined, [Validators.required]],
  });

  constructor(private fb: FormBuilder,
              private matSnackBar: MatSnackBar,
              private mascotaService: MascotaService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.miFormulario.reset({});
    this.miFormulario.untouched;
  }

  guardar() {
    // TODO - controlar si el form es valido
    if(this.miFormulario.invalid){
      this.miFormulario.markAllAsTouched
      return;
    }
    // TODO - crear el obj a enviar

    const mascota:Mascota=this.miFormulario.value;

    // TODO - invocar API
    this.mascotaService.guardarMascota(mascota).subscribe((resp:number)=>{
      this.showSnackBar("La mascota se registro con el ID: " + resp);
      this.router.navigate(['mascotas/listado']);
    },error=>{
      this.showSnackBar(error.error);
    })

    // TODO - mostrar mensaje correspondiente con el snackBar

    // TODO - navegacion
  }

  showSnackBar(mensaje: string) {
    this.matSnackBar.open(mensaje, 'Aceptar', {
      duration: 3000
    });
  }

  cancelar() {
    this.router.navigate(['mascotas/listado']);
  }
}
