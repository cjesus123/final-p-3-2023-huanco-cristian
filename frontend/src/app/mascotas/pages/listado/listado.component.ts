import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Mascota } from "../../interfaces/mascota.interface";
import { MascotaService } from "../../services/mascota.service";
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nombre', 'fecha_nacimiento'];
  mascotas: Mascota[] = [];

  constructor(private router: Router,
    private mascotaService: MascotaService) {
  }

  ngOnInit(): void {
    this.mascotaService.getMascotas().subscribe((resp: Mascota[]) => {
      this.mascotas = resp;
    })
  }

  addData() {
    this.router.navigate(['mascotas/mascota']);
    // TODO - navegacion
  }
}
