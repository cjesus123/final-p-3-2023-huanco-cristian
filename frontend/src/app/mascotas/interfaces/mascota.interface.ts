export interface Mascota {
  id?: number;
  nombre: string;
  fecha_nacimiento: Date;
}
