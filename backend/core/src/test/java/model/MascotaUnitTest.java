package model;

import mascota.exception.MascotaIncompletaException;
import mascota.exception.MascotaIncorrectaException;
import mascota.modelo.Mascota;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class MascotaUnitTest {

    @Test
    void instancia_TodosLosAtributos_InstanciaCorrecta() throws MascotaIncompletaException {
        Mascota unaMascota = Mascota.instancia(1, "nombre", LocalDate.MIN);
        assertNotNull(unaMascota);
    }

    @Test
    void instancia_FaltaNombre_MascotaIncompletaException() {
        Exception exceptionNull = assertThrows(MascotaIncompletaException.class, () -> Mascota.instancia(1, null, LocalDate.MIN));
        Exception exceptionVacio = assertThrows(MascotaIncompletaException.class, () -> Mascota.instancia(1, "", LocalDate.MIN));
        assertEquals("El nombre de la mascota es obligatorio", exceptionNull.getMessage());
        assertEquals("El nombre de la mascota es obligatorio", exceptionVacio.getMessage());
    }

    @Test
    void instancia_FaltaFechaNacimiento_MascotaIncompletaException() {
        Exception exceptionNull = assertThrows(MascotaIncompletaException.class, () -> Mascota.instancia(1, "nombre", null));
        assertEquals("La fecha de nacimiento de la mascota es obligatoria", exceptionNull.getMessage());
    }

    @Test
    public void crearMascota_FechaSuperior_MascotaIncorrectaException() {
        Exception exceptionMalformed = (Exception) assertThrows(MascotaIncorrectaException.class, () -> Mascota.instancia(null, "max", LocalDate.MAX));
        assertEquals("La fecha de nacimiento de la mascota no puede ser superior a la actual", exceptionMalformed.getMessage());
    }
}
