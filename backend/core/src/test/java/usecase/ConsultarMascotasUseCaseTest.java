package usecase;

import factory.FactoryMascotas;
import mascota.input.IConsultarMascotasInput;
import mascota.modelo.Mascota;
import mascota.output.IConsultarMascotasRepositorio;
import mascota.usecase.ConsultarMascotaUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ConsultarMascotasUseCaseTest {

    IConsultarMascotasInput consultarMascotasInput;

    @Mock
    IConsultarMascotasRepositorio consultarMascotasRepositorio;

    @BeforeEach
    void setup() {
        consultarMascotasInput = new ConsultarMascotaUseCase(consultarMascotasRepositorio);
    }

    @Test
    public void consultarMascotas_ExistenMascotas_DevuelveColeccion() {
        when(consultarMascotasRepositorio.obtenerMascotas()).thenReturn(FactoryMascotas.sampleMany(2));
        Collection<Mascota> mascotas = consultarMascotasInput.consultarMascotas();
        assertEquals(2, mascotas.size());
    }

    @Test
    public void consultarMascotas_ExistenMascotas_DevuelveColeccionVacia() {
        when(consultarMascotasRepositorio.obtenerMascotas()).thenReturn(Collections.emptyList());
        Collection<Mascota> mascotas = consultarMascotasInput.consultarMascotas();
        assertEquals(0, mascotas.size());
    }
}
