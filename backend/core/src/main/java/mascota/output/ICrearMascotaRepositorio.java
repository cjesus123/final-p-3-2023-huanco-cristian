package mascota.output;

import mascota.modelo.Mascota;

public interface ICrearMascotaRepositorio {
    boolean existePorNombre(String nombre);

    Integer guardar(Mascota nuevaMascota);
}
