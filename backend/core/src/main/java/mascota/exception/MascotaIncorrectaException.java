package mascota.exception;

public class MascotaIncorrectaException extends RuntimeException{
    public MascotaIncorrectaException(String msg){
        super(msg);
    }
}
