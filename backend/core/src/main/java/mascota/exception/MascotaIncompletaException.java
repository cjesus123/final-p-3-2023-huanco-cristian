package mascota.exception;

public class MascotaIncompletaException extends RuntimeException{
    public MascotaIncompletaException(String msg){
        super(msg);
    }
}
