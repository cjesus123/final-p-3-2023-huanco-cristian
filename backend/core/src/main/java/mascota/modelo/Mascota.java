package mascota.modelo;

import mascota.exception.MascotaIncompletaException;
import mascota.exception.MascotaIncorrectaException;

import java.time.LocalDate;

public class Mascota {
    private Integer id;
    private String nombre;
    private LocalDate fecha_nacimiento;

    private Mascota(Integer id, String nombre, LocalDate fecha_nacimiento) {
        this.id = id;
        this.nombre = nombre;
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public static Mascota instancia(Integer id,String nombre,LocalDate fecha_nacimiento){
        if(nombre == null || nombre.isBlank())
            throw new MascotaIncompletaException("El nombre de la mascota es obligatorio");
        if(fecha_nacimiento == null)
            throw new MascotaIncompletaException("La fecha de nacimiento de la mascota es obligatoria");
        if(fecha_nacimiento.isAfter(LocalDate.now()))
            throw new MascotaIncorrectaException("La fecha de nacimiento de la mascota no puede ser superior a la actual");
        return new Mascota(id,nombre,fecha_nacimiento);
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public LocalDate getFecha_nacimiento() {
        return fecha_nacimiento;
    }
}
