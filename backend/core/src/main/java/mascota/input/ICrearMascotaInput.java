package mascota.input;

import mascota.modelo.Mascota;

public interface ICrearMascotaInput {
    Integer crearMascota(Mascota nuevaMascota);
}
