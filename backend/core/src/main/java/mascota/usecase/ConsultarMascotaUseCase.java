package mascota.usecase;

import mascota.input.IConsultarMascotasInput;
import mascota.modelo.Mascota;
import mascota.output.IConsultarMascotasRepositorio;

import java.util.Collection;

public class ConsultarMascotaUseCase implements IConsultarMascotasInput{

    IConsultarMascotasRepositorio iConsultarMascotasRepositorio;
    public ConsultarMascotaUseCase(IConsultarMascotasRepositorio consultarMascotasRepositorio) {
        this.iConsultarMascotasRepositorio = consultarMascotasRepositorio;
    }

    @Override
    public Collection<Mascota> consultarMascotas() {
        return iConsultarMascotasRepositorio.obtenerMascotas();
    }
}
