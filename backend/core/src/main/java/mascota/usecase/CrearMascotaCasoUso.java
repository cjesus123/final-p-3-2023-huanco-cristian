package mascota.usecase;

import mascota.exception.MascotaExisteException;
import mascota.input.ICrearMascotaInput;
import mascota.modelo.Mascota;
import mascota.output.ICrearMascotaRepositorio;

public class CrearMascotaCasoUso implements ICrearMascotaInput{

    ICrearMascotaRepositorio iCrearMascotaRepositorio;
    public CrearMascotaCasoUso(ICrearMascotaRepositorio iCrearMascotaRepositorio) {
        this.iCrearMascotaRepositorio = iCrearMascotaRepositorio;
    }

    public Integer crearMascota(Mascota nuevaMascota){
        if(iCrearMascotaRepositorio.existePorNombre(nuevaMascota.getNombre()))
            throw new MascotaExisteException("Ya existe una mascota con nombre " + nuevaMascota.getNombre());
        return iCrearMascotaRepositorio.guardar(nuevaMascota);
    }
}
