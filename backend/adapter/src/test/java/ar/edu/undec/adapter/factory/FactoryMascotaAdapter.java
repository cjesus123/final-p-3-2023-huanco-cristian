package ar.edu.undec.adapter.factory;

import ar.edu.undec.adapter.data.mascota.model.MascotaEntity;
import ar.edu.undec.adapter.service.mascota.model.MascotaDTO;
import mascota.modelo.Mascota;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FactoryMascotaAdapter {

    private FactoryMascotaAdapter() {
    }

    public static MascotaEntity sampleEntity(Integer id) {
        return new MascotaEntity(id, "max" + id, LocalDate.MIN);
    }

    public static Collection<MascotaEntity> sampleManyEntity(Integer cantidad) {
        return IntStream.range(0, cantidad).mapToObj(FactoryMascotaAdapter::sampleEntity).collect(Collectors.toList());
    }


    public static MascotaDTO sampleDTO(Integer id) {
        return new MascotaDTO(id, "max" + id, LocalDate.MIN);
    }

    public static Collection<MascotaDTO> sampleManyDTO(Integer cantidad) {
        return IntStream.range(0, cantidad).mapToObj(FactoryMascotaAdapter::sampleDTO).collect(Collectors.toList());
    }

    public static Mascota sampleCore(Integer id) {
        return Mascota.instancia(id, "max" + id, LocalDate.MIN);
    }

    public static Collection<Mascota> sampleManyCore(Integer cantidad) {
        return IntStream.range(0, cantidad).mapToObj(FactoryMascotaAdapter::sampleCore).collect(Collectors.toList());
    }
}

