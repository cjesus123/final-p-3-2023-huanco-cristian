package ar.edu.undec.adapter.data.mascota;

import ar.edu.undec.adapter.data.exception.DataBaseException;
import ar.edu.undec.adapter.data.mascota.crud.ICrearMascotaCRUD;
import ar.edu.undec.adapter.data.mascota.model.MascotaEntity;
import ar.edu.undec.adapter.data.mascota.repoimplementacion.CrearMascotaRepositorioImplementacion;
import ar.edu.undec.adapter.factory.FactoryMascotaAdapter;
import mascota.modelo.Mascota;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CrearMascotaDataTest {

    @InjectMocks
    CrearMascotaRepositorioImplementacion crearMascotaRepositorioImplementacion;

    @Mock
    ICrearMascotaCRUD crearMascotaCrud;

    @Test
    public void crearMascota_ProcesoCorrecto_DevuelveID() {
        Mascota mascotaCore = FactoryMascotaAdapter.sampleCore(null);
        when(crearMascotaCrud.save(any(MascotaEntity.class))).thenReturn(FactoryMascotaAdapter.sampleEntity(1));
        Integer nuevoId = crearMascotaRepositorioImplementacion.guardar(mascotaCore);
        assertEquals(1, nuevoId);
    }

    @Test
    public void crearMascota_FallaBaseDeDatos_DatabaseException() {
        doThrow(RuntimeException.class).when(crearMascotaCrud).save(any(MascotaEntity.class));
        Mascota mascotaCore = FactoryMascotaAdapter.sampleCore(null);
        assertThrows(DataBaseException.class, () -> crearMascotaRepositorioImplementacion.guardar(mascotaCore));
    }

    @Test
    public void existePorNombre_Existe_DevuelveTrue() {
        Mascota mascotaCore = FactoryMascotaAdapter.sampleCore(null);
        when(crearMascotaCrud.existsByNombre("maxnull")).thenReturn(true);
        Boolean resultado = crearMascotaRepositorioImplementacion.existePorNombre(mascotaCore.getNombre());
        assertTrue(resultado);
    }

    @Test
    public void existePorNombre_NoExiste_DevuelveFalse() {
        Mascota mascotaCore = FactoryMascotaAdapter.sampleCore(null);
        when(crearMascotaCrud.existsByNombre("maxnull")).thenReturn(false);
        Boolean resultado = crearMascotaRepositorioImplementacion.existePorNombre(mascotaCore.getNombre());
        assertFalse(resultado);
    }
}
