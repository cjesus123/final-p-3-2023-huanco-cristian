package ar.edu.undec.adapter.service.mascota;

import ar.edu.undec.adapter.factory.FactoryMascotaAdapter;
import ar.edu.undec.adapter.service.mascota.controller.CrearMascotaController;
import ar.edu.undec.adapter.service.mascota.model.MascotaDTO;
import mascota.exception.MascotaExisteException;
import mascota.input.ICrearMascotaInput;
import mascota.modelo.Mascota;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
public class CrearMascotaServiceTest {

    @InjectMocks
    CrearMascotaController crearMascotaController;

    @Mock
    ICrearMascotaInput crearMascotaInput;

    @BeforeEach
    void setup() {
        crearMascotaController = new CrearMascotaController(crearMascotaInput);
    }

    @Test
    public void crearMascota_ProcesoCorrecto_Devuelve201Created() {
        MascotaDTO nuevaMascota = FactoryMascotaAdapter.sampleDTO(null);
        when(crearMascotaInput.crearMascota(any(Mascota.class))).thenReturn(1);
        ResponseEntity<?> response = crearMascotaController.crearMascota(nuevaMascota);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(1, response.getBody());
    }

    @Test
    public void crearMascota_MascotaExiste_Devuelve400BadRequest() {
        MascotaDTO nuevaMascota = FactoryMascotaAdapter.sampleDTO(null);
        doThrow(MascotaExisteException.class).when(crearMascotaInput).crearMascota(any(Mascota.class));
        ResponseEntity<?> response = crearMascotaController.crearMascota(nuevaMascota);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
}
