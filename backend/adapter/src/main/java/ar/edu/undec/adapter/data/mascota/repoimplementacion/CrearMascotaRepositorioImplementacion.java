package ar.edu.undec.adapter.data.mascota.repoimplementacion;

import ar.edu.undec.adapter.data.exception.DataBaseException;
import ar.edu.undec.adapter.data.mascota.crud.ICrearMascotaCRUD;
import ar.edu.undec.adapter.data.mascota.mapper.MascotaDataMapper;
import ar.edu.undec.adapter.data.mascota.model.MascotaEntity;
import mascota.modelo.Mascota;
import mascota.output.ICrearMascotaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.zip.DataFormatException;

@Service
public class CrearMascotaRepositorioImplementacion implements ICrearMascotaRepositorio {

    ICrearMascotaCRUD crearMascotaCRUD;

    @Autowired
    public CrearMascotaRepositorioImplementacion(ICrearMascotaCRUD crearMascotaCRUD) {
        this.crearMascotaCRUD = crearMascotaCRUD;
    }

    @Override
    public boolean existePorNombre(String nombre) {
        return crearMascotaCRUD.existsByNombre(nombre);
    }

    @Override
    public Integer guardar(Mascota nuevaMascota) {
        try{
            return crearMascotaCRUD.save(MascotaDataMapper.coreToData(nuevaMascota)).getId();
        }catch (RuntimeException e){
            throw new DataBaseException("Error al crear la mascota");
        }
    }
}
