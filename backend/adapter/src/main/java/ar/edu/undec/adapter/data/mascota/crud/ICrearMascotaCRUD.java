package ar.edu.undec.adapter.data.mascota.crud;

import ar.edu.undec.adapter.data.mascota.model.MascotaEntity;
import mascota.modelo.Mascota;
import org.springframework.data.repository.CrudRepository;

public interface ICrearMascotaCRUD extends CrudRepository<MascotaEntity,Integer>{

    boolean existsByNombre(String nombre);
    MascotaEntity save(MascotaEntity mascotaEntity);
}
