package ar.edu.undec.adapter.data.mascota.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "mascotas")
public class MascotaEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String nombre;
    private LocalDate fecha_nacimiento;

    public MascotaEntity(){}
    public MascotaEntity(Integer id, String nombre, LocalDate fecha_nacimiento) {
        this.id = id;
        this.nombre = nombre;
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public LocalDate getFecha_nacimiento() {
        return fecha_nacimiento;
    }
}
