package ar.edu.undec.adapter.service.mascota.mapper;

import ar.edu.undec.adapter.service.mascota.model.MascotaDTO;
import mascota.modelo.Mascota;

public class MascotaServiceMapper{

    public static Mascota DTOCore(MascotaDTO mascotaDTO){
        try{
            return Mascota.instancia(mascotaDTO.getId(), mascotaDTO.getNombre(), mascotaDTO.getFecha_nacimiento());
        }catch (RuntimeException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public static MascotaDTO coreDTO(Mascota mascota){
        try{
            return new MascotaDTO(mascota.getId(), mascota.getNombre(), mascota.getFecha_nacimiento());
        }catch (RuntimeException e){
            throw new RuntimeException(e.getMessage());
        }
    }
}
