package ar.edu.undec.adapter.data.mascota.mapper;

import ar.edu.undec.adapter.data.exception.FailedMapperException;
import ar.edu.undec.adapter.data.mascota.model.MascotaEntity;
import mascota.modelo.Mascota;

public class MascotaDataMapper {

    public static MascotaEntity coreToData(Mascota mascota){
        try{
            return new MascotaEntity(mascota.getId(),mascota.getNombre(),mascota.getFecha_nacimiento());
        }catch (RuntimeException e){
            throw new FailedMapperException("No se puede Mapear de core a data");
        }
    }

    public static Mascota dataToCore(MascotaEntity mascota){
        try{
            return Mascota.instancia(mascota.getId(),mascota.getNombre(),mascota.getFecha_nacimiento());
        }catch (RuntimeException e){
            throw new FailedMapperException("No se puede Mapear de data a core");
        }
    }
}
