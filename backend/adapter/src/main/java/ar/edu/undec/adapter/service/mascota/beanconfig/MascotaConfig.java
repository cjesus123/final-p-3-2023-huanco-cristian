package ar.edu.undec.adapter.service.mascota.beanconfig;

import mascota.input.IConsultarMascotasInput;
import mascota.input.ICrearMascotaInput;
import mascota.output.IConsultarMascotasRepositorio;
import mascota.output.ICrearMascotaRepositorio;
import mascota.usecase.ConsultarMascotaUseCase;
import mascota.usecase.CrearMascotaCasoUso;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MascotaConfig {

    @Bean
    public ICrearMascotaInput iCrearMascotaInput(ICrearMascotaRepositorio iCrearMascotaRepositorio){
        return new CrearMascotaCasoUso(iCrearMascotaRepositorio);
    }

    @Bean
    public IConsultarMascotasInput iConsultarMascotasInput(IConsultarMascotasRepositorio iConsultarMascotasRepositorio){
        return new ConsultarMascotaUseCase(iConsultarMascotasRepositorio);
    }
}
