package ar.edu.undec.adapter.service.mascota.controller;

import ar.edu.undec.adapter.service.mascota.mapper.MascotaServiceMapper;
import ar.edu.undec.adapter.service.mascota.model.MascotaDTO;
import mascota.input.ICrearMascotaInput;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("mascotas")
@CrossOrigin(origins = "*")
public class CrearMascotaController {

    ICrearMascotaInput crearMascotaInput;

    public CrearMascotaController(ICrearMascotaInput crearMascotaInput) {
        this.crearMascotaInput = crearMascotaInput;
    }

    @PostMapping()
    public ResponseEntity<?> crearMascota(@RequestBody MascotaDTO mascotaDTO){
        try{
            return ResponseEntity.created(null).body(crearMascotaInput.crearMascota(MascotaServiceMapper.DTOCore(mascotaDTO)));
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
