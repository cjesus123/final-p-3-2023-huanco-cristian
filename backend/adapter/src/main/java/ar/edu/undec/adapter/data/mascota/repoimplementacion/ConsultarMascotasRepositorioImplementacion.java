package ar.edu.undec.adapter.data.mascota.repoimplementacion;

import ar.edu.undec.adapter.data.mascota.crud.IConsultarMascotasCRUD;
import ar.edu.undec.adapter.data.mascota.mapper.MascotaDataMapper;
import mascota.modelo.Mascota;
import mascota.output.IConsultarMascotasRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class ConsultarMascotasRepositorioImplementacion implements IConsultarMascotasRepositorio {

    IConsultarMascotasCRUD consultarMascotasCRUD;

    @Autowired
    public ConsultarMascotasRepositorioImplementacion(IConsultarMascotasCRUD consultarMascotasCRUD) {
        this.consultarMascotasCRUD = consultarMascotasCRUD;
    }

    @Override
    public Collection<Mascota> obtenerMascotas() {
        return consultarMascotasCRUD.findAll().stream().map(MascotaDataMapper::dataToCore).collect(Collectors.toList());
    }
}
