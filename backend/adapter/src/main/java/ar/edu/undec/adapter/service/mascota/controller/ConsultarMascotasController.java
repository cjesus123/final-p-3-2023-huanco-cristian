package ar.edu.undec.adapter.service.mascota.controller;

import ar.edu.undec.adapter.service.mascota.mapper.MascotaServiceMapper;
import ar.edu.undec.adapter.service.mascota.model.MascotaDTO;
import mascota.input.IConsultarMascotasInput;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("mascotas")
@CrossOrigin(origins = "*")
public class ConsultarMascotasController {
    IConsultarMascotasInput iConsultarMascotasInput;

    public ConsultarMascotasController(IConsultarMascotasInput iConsultarMascotasInput) {
        this.iConsultarMascotasInput = iConsultarMascotasInput;
    }

    @GetMapping
    public ResponseEntity<List<MascotaDTO>> consultarMascotas(){
        if(iConsultarMascotasInput.consultarMascotas().isEmpty())
            return ResponseEntity.noContent().build();
        return ResponseEntity.ok(iConsultarMascotasInput.consultarMascotas().stream().map(MascotaServiceMapper::coreDTO).collect(Collectors.toList()));
    }
}
